"""OBS Source borders. Automatically create borders for sources."""


import datetime
import obspython as obs
import re
import socket
import threading
import time


BORDER_SOURCE_SUFFIX = "_border"
LAST_MESSAGE = None
MAX_SOURCES_WITH_BORDERS_BY_NAME_COUNT = 100
SCENE_ORDER_HANDLERS_BY_NAME = None
SETTINGS = []
SOURCES_WITH_BORDERS_BY_NAME = None
SOURCES_WITH_BORDERS_BY_NAME_COUNT = 0

THREAD_LOCK = threading.Lock()
FRAME_TIME = 100

ACTIONS = []
ACTION_APPLYNEWSORTORDER = "ApplyNewSortOrder"
ACTION_SETBORDER = "SetBorder"
ACTION_SETUP = "Setup"
ACTION_SETUPBORDERS = "SetupBorders"
ACTION_SETUPHANDLERS = "SetupHandlers"


class SceneOrderHandler(object):
    name = None
    orderIndexBySourceNames = None
    requiresNewSortOrder = None
    handlersSetup = None

    def __init__(self, name):
        self.name = name
        self.orderIndexBySourceNames = {}
        self.requiresNewSortOrder = False
        self.handlersSetup = False

    def __repr__(self):
        return "<SceneOrderHandler \"%s\">" % self.name

    def AddSource(self, name, index):
        self.orderIndexBySourceNames[name] = index
    
    def ApplyNewSortOrder(self):
        if not self.requiresNewSortOrder:
            return

        sceneSource = None
        try:
            sceneSource = obs.obs_get_source_by_name(self.name)
            scene = obs.obs_scene_from_source(sceneSource)

            sortedItems = sorted((i, s) for s, i in self.orderIndexBySourceNames.items())
            for index, sourceName in sortedItems:
                source = None
                sceneItem = None
                try:
                    # Even though this source isn't used, there is some issue
                    # with reference tracking the sourceitem. It needs to be
                    # retrieved here and released.
                    source = obs.obs_get_source_by_name(sourceName)

                    sceneItem = obs.obs_scene_find_source(scene, sourceName)
                    obs.obs_sceneitem_addref(sceneItem)
                    obs.obs_sceneitem_set_order_position(sceneItem, index)
                finally:
                    obs.obs_sceneitem_release(sceneItem)
                    obs.obs_source_release(source)
        finally:
            obs.obs_source_release(sceneSource)

        self.requiresNewSortOrder = False

    def SetupHandlers(self):
        if self.handlersSetup:
            return

        sceneSource = None
        try:
            sceneSource = obs.obs_get_source_by_name(self.name)
            signalHandler = obs.obs_source_get_signal_handler(sceneSource)
            # obs.signal_handler_connect(signalHandler, "item_add", ItemAdded)
            obs.signal_handler_connect(signalHandler, "item_visible", ItemUpdated)
            # obs.signal_handler_connect(signalHandler, "item_transform", ItemUpdated)
            # obs.signal_handler_connect(signalHandler, "item_select", ItemUpdated)
            # obs.signal_handler_connect(signalHandler, "item_deselect", ItemUpdated)
        finally:
            obs.obs_source_release(sceneSource)

        self.handlersSetup = True

    def InsertSourceAfterSource(self, newSourceName, sourceToInsertAfter):
        self.requiresNewSortOrder = True

        for sourceName, index in self.orderIndexBySourceNames.items():
            if sourceName == sourceToInsertAfter:
                newIndex = index
                break

        for sourceName, index in self.orderIndexBySourceNames.items():
            if index >= newIndex:
                self.orderIndexBySourceNames[sourceName] += 1

        self.orderIndexBySourceNames[newSourceName] = newIndex
    

class SourceWithBorder(object):
    sourceName = None
    sceneName = None
    borderColor = None
    borderWidth = None
    lastTransform = None
    lastVisible = None
    locked = None

    def __init__(self, sourceName, sceneName, borderColor, borderWidth, sortIndex):
        self.sourceName = sourceName
        self.sceneName = sceneName
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.sortIndex = sortIndex

        self.locked = False

    def __repr__(self):
        return "<SourceWithBorder \"%s\">" % self.sourceName

    @property
    def colorSourceName(self):
        global BORDER_SOURCE_SUFFIX
        return "%s%s" % (self.sourceName, BORDER_SOURCE_SUFFIX)

    @property
    def sourceSeqNum(self):
        return None

    def SetBorder(self):
        sourceSceneItem = None
        sceneSource = None
        colorSceneItem = None
        try:
            sceneSource = obs.obs_get_source_by_name(self.sceneName)
            scene = obs.obs_scene_from_source(sceneSource)
            source = obs.obs_get_source_by_name(self.sourceName)
            sourceSceneItem = obs.obs_scene_find_source(scene, self.sourceName)
            obs.obs_sceneitem_addref(sourceSceneItem)

            # Add a reference to the item, there is a bug in obs_scene_find_item.
            colorSceneItem = obs.obs_scene_find_source(scene, self.colorSourceName)
            obs.obs_sceneitem_addref(colorSceneItem)

            position = obs.vec2()
            scale = obs.vec2()
            crop = obs.obs_sceneitem_crop()
            transform = obs.obs_transform_info()

            width = obs.obs_source_get_width(source)
            height = obs.obs_source_get_height(source)
            visible = obs.obs_sceneitem_visible(sourceSceneItem)

            obs.obs_sceneitem_get_pos(sourceSceneItem, position)
            obs.obs_sceneitem_get_scale(sourceSceneItem, scale)
            obs.obs_sceneitem_get_crop(sourceSceneItem, crop)
            obs.obs_sceneitem_get_info(sourceSceneItem, transform)
            
            scaledWidth = round((width - crop.left - crop.right) * transform.scale.x)
            scaledHeight = round((height - crop.top - crop.bottom) * transform.scale.y)

            position.x -= self.borderWidth
            position.y -= self.borderWidth

            transform.pos.x -= self.borderWidth
            transform.pos.y -= self.borderWidth
            transform.bounds_type = obs.OBS_BOUNDS_STRETCH 
            transform.bounds.x = scaledWidth + 2*self.borderWidth
            transform.bounds.y += scaledHeight + 2*self.borderWidth

            newTransform = (transform.pos.x, transform.pos.y, transform.bounds.x, transform.bounds.y)
            if newTransform != self.lastTransform:
                obs.obs_sceneitem_set_info(colorSceneItem, transform)
                self.lastTransform = newTransform

            if visible != self.lastVisible:
                obs.obs_sceneitem_set_visible(colorSceneItem, visible)
                self.lastVisible = visible

            if not self.locked:
                obs.obs_sceneitem_set_locked(colorSceneItem, True)
                self.locked = True

        finally:
            obs.obs_source_release(sceneSource)
            obs.obs_source_release(source)
            obs.obs_sceneitem_release(sourceSceneItem)
            obs.obs_sceneitem_release(colorSceneItem)

    def CreateBorderColorSource(self):
        global SCENE_ORDER_HANDLERS_BY_NAME

        existingColorSource = None
        scene = None
        sceneSource = None
        newSceneItem = None
        colorSource = None
        settingsData = None
        hotkeyData = None
        try:
            sceneSource = obs.obs_get_source_by_name(self.sceneName)
            scene = obs.obs_scene_from_source(sceneSource)

            existingColorSource = obs.obs_get_source_by_name(self.colorSourceName)
            if existingColorSource:
                return

            settingsData = obs.obs_data_create()
            colorWithOpacity = self.borderColor
            if colorWithOpacity < 4278190080:
                colorWithOpacity += 4278190080

            obs.obs_data_set_double(settingsData, "color", colorWithOpacity)

            hotkeyData = obs.obs_data_create()
            colorSource = obs.obs_source_create(
                "color_source",
                self.colorSourceName,
                settingsData,
                hotkeyData,
            )

            newSceneItem = obs.obs_scene_add(scene, colorSource)
            obs.obs_sceneitem_addref(newSceneItem)

            sceneOrderHandler = SCENE_ORDER_HANDLERS_BY_NAME[self.sceneName]
            sceneOrderHandler.InsertSourceAfterSource(self.colorSourceName, self.sourceName)

        finally:
            obs.obs_source_release(existingColorSource)
            obs.obs_source_release(colorSource)
            obs.obs_data_release(settingsData)
            obs.obs_data_release(hotkeyData)
            obs.obs_sceneitem_release(newSceneItem)
            obs.obs_source_release(sceneSource)

    def Setup(self):
        self.CreateBorderColorSource()
        self.SetBorder()


def Initialize(settings):
    global SOURCES_WITH_BORDERS_BY_NAME_COUNT
    global MAX_SOURCES_WITH_BORDERS_BY_NAME_COUNT
    global SETTINGS

    for index in range(1, MAX_SOURCES_WITH_BORDERS_BY_NAME_COUNT):
        startingSymbols = obs.obs_data_get_string(settings, 
                "starting_symbols_%s" % index)

        if not startingSymbols:
            continue

        SOURCES_WITH_BORDERS_BY_NAME_COUNT = index
        borderWidth = obs.obs_data_get_int(settings, 
                "border_width_%s" % index)
        borderColor = obs.obs_data_get_double(settings, 
                "border_color_%s" % index)
        
        SETTINGS.append({
            "StartingSymbols": startingSymbols,
            "BorderWidth": borderWidth,
            "BorderColor": borderColor,
        })

LAST_MESSAGE_TIME = None
def print_message(messageText):
    global LAST_MESSAGE_TIME
    # global LAST_MESSAGE
    # if message == LAST_MESSAGE:
    #     return

    # LAST_MESSAGE = message
    
    messageTime = datetime.datetime.now()
    if LAST_MESSAGE_TIME:
        secondsSinceLast = (messageTime - LAST_MESSAGE_TIME).total_seconds()
    else:
        secondsSinceLast = ""

    LAST_MESSAGE_TIME = messageTime
    message = "[%s] %s" % (secondsSinceLast, messageText)
    print(message)

    with open("c:\\temp\\obs.log", 'a') as f:
        f.write("%s\n" % message)


# defines script description
def script_description():
    return "Automatically creates borders for sources that match the set " \
        "naming pattern set. To add more than one " \
        "setting, set the first one and hit 'Refresh'"


# defines user properties
def script_properties():
    properties = obs.obs_properties_create()

    # Get list of possible text fields for selection
    for index in range(1, SOURCES_WITH_BORDERS_BY_NAME_COUNT + 2):
        # Create properties
        obs.obs_properties_add_text(
            properties, 
            "starting_symbols_%s" % index, 
            "Starting Symbols %s" % index, 
            obs.OBS_TEXT_DEFAULT,
            )
        obs.obs_properties_add_float_slider(
            properties, 
            "border_width_%s" % index, 
            "Border Width %s" % index, 
            1, 
            50.0,
            1,
            )
        obs.obs_properties_add_color(
            properties, 
            "border_color_%s" % index, 
            "Border Color (%s)" % index,
            )

    return properties


# called when user updatas settings
def script_update(settings):
    Reset()
    Initialize(settings)
    SetupBorders()


def Reset():
    global SOURCES_WITH_BORDERS_BY_NAME
    global SCENE_ORDER_HANDLERS_BY_NAME

    SOURCES_WITH_BORDERS_BY_NAME = {}
    SCENE_ORDER_HANDLERS_BY_NAME = {}
    

def ItemAdded(calldata):
    # Get the item and queue it for refresh. The refresh can't be
    # done directly because there seems to be some reference count
    # issues that can't be overcome. Reseting the timer means that
    # the refresh will only be called once the item's transformation
    # is done.
    global ACTIONS
    global SOURCES_WITH_BORDERS_BY_NAME

    print_message("Item Added!")

    # Some actions trigger spurrious signals, ignore them if one is
    # already being handled.
    with THREAD_LOCK:
        sceneItem = obs.calldata_sceneitem(calldata, "item")
        scene = obs.obs_sceneitem_get_scene(sceneItem)
        sceneSource = obs.obs_scene_get_source(scene)
        sceneName = obs.obs_source_get_name(sceneSource)

        action = (ACTION_SETUPBORDERS, sceneName)
        print_message("Queuing action: %r" % (action, ))
        if action not in ACTIONS:
            ACTIONS = [action]
            
            # Reset timer
            obs.timer_remove(RunFrame)
            obs.timer_add(RunFrame, FRAME_TIME)
    

def ItemUpdated(calldata):
    # Get the item and queue it for refresh. The refresh can't be
    # done directly because there seems to be some reference count
    # issues that can't be overcome. Reseting the timer means that
    # the refresh will only be called once the item's transformation
    # is done.
    global ACTIONS
    global SOURCES_WITH_BORDERS_BY_NAME

    # Some actions trigger spurrious signals, ignore them if one is
    # already being handled.
    with THREAD_LOCK:
        # If there is already a SetupBorders queued, don't queue anything else.
        if ACTION_SETUPBORDERS in [a for a, t in ACTIONS]:
            return

        sceneItem = obs.calldata_sceneitem(calldata, "item")
        source = obs.obs_sceneitem_get_source(sceneItem)
        sourceName = obs.obs_source_get_name(source)
        sourceWithBorder = SOURCES_WITH_BORDERS_BY_NAME.get(sourceName)
        if sourceWithBorder:
            action = (ACTION_SETBORDER, sourceWithBorder)
            if action not in ACTIONS:
                ACTIONS.append(action)
                
                # Reset timer
                obs.timer_remove(RunFrame)
                obs.timer_add(RunFrame, FRAME_TIME)


def SetupBorders(initSceneName = None):
    global ACTIONS
    global BORDER_SOURCE_SUFFIX
    global SCENE_ORDER_HANDLERS_BY_NAME
    global SETTINGS
    global SOURCES_WITH_BORDERS_BY_NAME
    global THREAD_LOCK

    with THREAD_LOCK:
        for setting in SETTINGS:
            startingSymbols = setting["StartingSymbols"]
            borderWidth = setting["BorderWidth"]
            borderColor = setting["BorderColor"]

            allSceneSources = []
            try:
                # if initSceneName:
                #     allSceneSources = [obs.obs_get_source_by_name(initSceneName)]
                # else:
                #     allSceneSources = obs.obs_frontend_get_scenes()
                allSceneSources = obs.obs_frontend_get_scenes()

                for sceneSource in allSceneSources:
                    scene = obs.obs_scene_from_source(sceneSource)
                    sceneName = obs.obs_source_get_name(sceneSource)
                    
                    sceneOrderHandler = SCENE_ORDER_HANDLERS_BY_NAME.get(sceneName)
                    if not sceneOrderHandler:
                        sceneOrderHandler = SceneOrderHandler(sceneName)
                        SCENE_ORDER_HANDLERS_BY_NAME[sceneName] = sceneOrderHandler

                    sceneItems = []
                    try:
                        sceneItems = obs.obs_scene_enum_items(scene)
                        for index, sceneItem in enumerate(sceneItems):
                            source = obs.obs_sceneitem_get_source(sceneItem)
                            sourceName = obs.obs_source_get_name(source)

                            if sourceName in SOURCES_WITH_BORDERS_BY_NAME:
                                continue

                            sceneOrderHandler.AddSource(sourceName, index)
                            
                            if (sourceName.upper().startswith(startingSymbols.upper())
                                and not sourceName.endswith(BORDER_SOURCE_SUFFIX)
                            ):
                                newSourceWithBorder = SourceWithBorder(
                                    sourceName,
                                    sceneName,
                                    borderColor,
                                    borderWidth,
                                    index,
                                )
                                SOURCES_WITH_BORDERS_BY_NAME[sourceName] = newSourceWithBorder

                    finally:
                        obs.sceneitem_list_release(sceneItems)
            finally:
                obs.source_list_release(allSceneSources)

        ACTIONS += [(ACTION_SETUP, s) for s in SOURCES_WITH_BORDERS_BY_NAME.values()]
        ACTIONS += [(ACTION_APPLYNEWSORTORDER, s) for s in SCENE_ORDER_HANDLERS_BY_NAME.values()]
        ACTIONS += [(ACTION_SETUPHANDLERS, s) for s in SCENE_ORDER_HANDLERS_BY_NAME.values()]

        if ACTIONS:
            obs.timer_remove(RunFrame)
            obs.timer_add(RunFrame, FRAME_TIME)
            

def RunFrame():
    global ACTIONS

    if THREAD_LOCK.acquire(blocking = False):
        try:
            obs.timer_remove(RunFrame)
            if not ACTIONS:
                return

            action = ACTIONS[0]
            ACTIONS = ACTIONS[1:]

            actionName, target = action
            if actionName == ACTION_SETUP:
                target.Setup()

            elif actionName == ACTION_SETBORDER:
                target.SetBorder()

            elif actionName == ACTION_SETUPBORDERS:
                Reset()
                # SetupBorders(target)
                SetupBorders()

            elif actionName == ACTION_APPLYNEWSORTORDER:
                target.ApplyNewSortOrder()

            elif actionName == ACTION_SETUPHANDLERS:
                target.SetupHandlers()

            if ACTIONS:
                obs.timer_add(RunFrame, FRAME_TIME)

        finally:
            THREAD_LOCK.release()
